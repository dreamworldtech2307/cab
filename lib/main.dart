import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:slide_popup_dialog/slide_popup_dialog.dart' as slideDialog;
import 'package:slide_to_act/slide_to_act.dart';
import 'package:slide_to_act/slide_to_act.dart';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/services.dart';
import 'package:swipe_to/swipe_to.dart';
import 'swipe_button.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Color(0xFF263238), accentColor: Color(0xFF263238)),
      home: Myapp(),
    ));

class Myapp extends StatefulWidget {
  @override
  _ListenLocationState createState() => _ListenLocationState();
}

class ListenLocationWidget extends StatefulWidget {
  const ListenLocationWidget({Key key}) : super(key: key);

  @override
  _ListenLocationState createState() => _ListenLocationState();
}

class _ListenLocationState extends State<Myapp> {
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  LatLng _initialPosition = LatLng(37.42796133580664, -122.085749655962);
  GoogleMapController _controller;
  BitmapDescriptor _markerIcon;
  List <Marker> allmarker=[];
  String icon = "assets/cab.png";
  double windowWidth = 0.0;
  double windowHeight = 0.0;

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec =
    await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration =
      createLocalImageConfiguration(context, size: Size.square(48));
      BitmapDescriptor.fromAssetImage(
          imageConfiguration, 'assets/cab.png')
          .then(_updateBitmap);
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    setState(() {
      _markerIcon = bitmap;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    allmarker.add(Marker(
      markerId: MarkerId('myMarker'),
      draggable: true,
      onTap: (){
        print('Marrker is tapped');
      },
      position: LatLng(10.6089, 78.4233),
      icon: _markerIcon,
    ));
  }


  Set<Marker> _createMarker() {
    // TODO(iskakaushik): Remove this when collection literals makes it to stable.
    // https://github.com/flutter/flutter/issues/28312
    // ignore: prefer_collection_literals
    return <Marker>[
      Marker(
        markerId: MarkerId("marker_1"),
        position: _initialPosition,
        icon: _markerIcon,
      ),
    ].toSet();
  }

  //Location _location = Location();

  final Location location = Location();

  LocationData _location;
  StreamSubscription<LocationData> _locationSubscription;
  String _error;

  Future<void> _listenLocation(GoogleMapController _cntrl) async {
    _controller = _cntrl;
    _locationSubscription =
        location.onLocationChanged.handleError((dynamic err) {
      setState(() {
        _error = err.code;
      });
      _locationSubscription.cancel();
    }).listen((LocationData currentLocation) {
      setState(() {
        _error = null;
        LatLng _kMapCenter = LatLng(52.4478, -3.5402);
        _location = currentLocation;
        _controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target:
                  LatLng(currentLocation.latitude, currentLocation.longitude),
              zoom: 17,

            ),
          ),
        );
        allmarker.add(Marker(
          markerId: MarkerId('myMarker'),
          draggable: true,
          onTap: (){
            print('Marrker is tapped');
          },
          position: LatLng(currentLocation.latitude, currentLocation.longitude),

        ));
      });
    });
  }

  Future<void> _stopListen() async {
    _locationSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    windowWidth = MediaQuery.of(context).size.width;
    windowHeight = MediaQuery.of(context).size.height;
    _createMarkerImageFromAsset(context);
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              print('cab');
            },
          ),
          //  title: Text(' Driver App'),
          actions: [
            IconButton(
              iconSize: 40,
              icon: Icon(Icons.notifications_none),
              onPressed: () {
                print('cab');
              },
            )
          ],
          bottom: PreferredSize(
              child: Container(
                  margin: new EdgeInsets.only(right: 20, bottom: 20),
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        children: [
                          Text(
                            '$textHolder',
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                      Switch(
                        onChanged: toggleSwitch,
                        value: switchControl,
                        activeColor: Colors.blue,
                        activeTrackColor: Colors.green,
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: Colors.grey,
                       ),
                    ],
                  )),
              preferredSize: Size.fromHeight(75.0)),
        ),
        body: Stack(
          children: <Widget>[
            Center(
              child: GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: _initialPosition,
                  zoom: 17.5,
                ),
                mapType: MapType.terrain,
                onMapCreated: _listenLocation,
                myLocationEnabled: true,
                markers: Set.from(allmarker),
              ),
            ),
            Container(
              child: Row(
                children: [],
              ),
            ),
            Container(
              //  padding: EdgeInsets.only(top: 60, bottom: 0),
              child: SlidingUpPanel(
                backdropEnabled: true,
                minHeight: 100.0,
                panel: Center(),
                collapsed: Container(
                  padding: EdgeInsets.only(top: 0,left: 40),
                    decoration: BoxDecoration(
                        color: Color(0xFF263238),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0))),
                    child: Center(
                      child: Row(
                        children: [
                          Text(
                            "You are $textHolder",
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                color: Colors.pink),
                          ),
                          Container(
                            child:Switch(
                              onChanged: toggleSwitch,
                              value: switchControl,
                              activeColor: Colors.blue,
                              activeTrackColor: Colors.green,
                              inactiveThumbColor: Colors.white,
                              inactiveTrackColor: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                ),
              ),
            ),
          ],
        ));
  }

  void _showDialog() {
    slideDialog.showSlideDialog(
      context: context,
      child: Container(
        padding: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Text("You Got a Ride Now",style: TextStyle(color: Colors.blue),
                ),
            Padding(
              padding: EdgeInsets.only(top: 100),
              child: SwipeButton(

                content: Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left:10 ),
                        child: Text(
                          'Cancel',
                          style: TextStyle(color: Colors.red,fontSize: 20),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left:100),
                        child:
                        Text(
                          'Ride',
                          style: TextStyle(color: Colors.green,fontSize: 20),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),

                    ],
                  ),
                ),

                onChanged: (result) {
                  if (result == SwipePosition.SwipeRight) {
                    //Navigator.pushNamed(context, pageRoute);
                    _showDialog1();
                  } else {
                    _showDialog2();
                  }
                },
            ),
            ),
          ],
        ),
      ),
      barrierColor: Colors.white.withOpacity(0.7),
      pillColor: Colors.red,
      backgroundColor: Colors.white,
    );
  }

  void _showDialog2() {
    slideDialog.showSlideDialog(
      context: context,
      child: Container(
        padding: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(right: 0,left: 0,top: 50,bottom: 50),

              child: Card(
                color: Color(0xFF263238),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 30,bottom: 30),
                        child: Text("Your Ride Has Been Cancelled",
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.pinkAccent,
                            )),
                      ),
                    ],
                  ),
              ),
            ),
          ],
        ),
      ),
      barrierColor: Colors.white.withOpacity(0.7),
      pillColor: Colors.red,
      backgroundColor: Colors.white,
    );
  }

  void _showDialog1() {
    slideDialog.showSlideDialog(
        context: context,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Pick Your Ride",
                    style: TextStyle(
                      fontSize: 25,
                      color: Color(0xFF263238),
                    )),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Card(
              color: Color(0xFF263238),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Column(
                        children: <Widget>[
                          IconButton(
                            iconSize: 40,
                            icon: Icon(
                              Icons.directions_walk,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              print('cab');
                            },
                          ),
                          Text('12min',
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                              ))
                        ],
                      )),

                  Container(
                    margin:  EdgeInsets.only(left: 0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Trade center",
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.pinkAccent,
                              )),
                          Text(
                            "Pickup location",
                            style: TextStyle(
                              fontSize: 10,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      )),

                  Container(
                    margin: EdgeInsets.only(right: 10,top: 0,bottom: 0),
                    padding: EdgeInsets.only(right:10,left: 10,top: 0,bottom: 0),
                    color: Colors.white70,
                      child: Column(
                        children: <Widget>[
                          IconButton(
                            iconSize: 50,
                            icon: Icon(
                              Icons.arrow_circle_up_outlined,
                              color: Colors.blue,
                            ),
                            onPressed: () {
                              print('cab');
                            },
                          ),

                          Text('Navigate',
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.blueGrey,
                              ))
                        ],
                      ),
                  ),


                ],
              ),
            ),

            Card(
              color: Color(0xFF263238),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin:EdgeInsets.only(bottom: 10),
                      child: Text('Ride Accepted',style: TextStyle(color: Colors.grey))
                  ),

                  Container(
                   margin:  EdgeInsets.only(right: 0),
                   child:Row(
                     children: [
                       Container(
                         child: IconButton(
                         iconSize: 40,
                         icon: Icon(
                           Icons.call,
                           color: Colors.green,
                         ),
                         onPressed: () {
                           print('call');
                         },
                    ),
                       ),

                       Container(
                         margin: EdgeInsets.only(left: 90),
                       child: Column(
                             children: [
                               CircleAvatar(
                                   radius: 30,
                                   backgroundImage: NetworkImage('https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500')
                               ),
                               Text('Name',style: TextStyle(color: Colors.grey)),
                             ],
                           ),
                       ),

                       Container(
                         margin: EdgeInsets.only(left: 80),
                         child: IconButton(
                           iconSize: 40,
                           icon: Icon(
                             Icons.comment,
                             color: Colors.lightBlue,
                           ),
                           onPressed: () {
                             print('comment');
                           },
                         ),
                       ),
                     ],
                   ), ),

                  Container(
                    child: Row(
                      children: [
                        Container(
                          margin:EdgeInsets.only(top:50, bottom: 20,left: 50),
                          padding:EdgeInsets.only(top:0,bottom:0,right:10,left: 10),
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: new BorderRadius.circular(10),
                          ),
                          child: TextButton(child:Text('Cancel Trip',style: TextStyle(color: Colors.white),)
                          ),

                        ),
                        Container(
                          margin:EdgeInsets.only(top:50, bottom: 20,left: 20),
                          padding:EdgeInsets.only(top:0,bottom:0,right:40,left: 40),
                          decoration: BoxDecoration(
                            color: Colors.pinkAccent,
                            borderRadius: new BorderRadius.circular(10),
                          ),
                          child: TextButton(child:Text('Arrived',style: TextStyle(color: Colors.white),)),
                        ),
                      ],
                    ),
                  )

                ],
              ),
            ),
          ],

        ));
  }

  bool switchControl = false;
  var textHolder = 'Offline';

  void toggleSwitch(bool value) {
    if (switchControl == false) {
      setState(() {
        switchControl = true;
        textHolder = 'Online';
        _showDialog() ;
      });
      print('Switch is ON');
      // Put your code here which you want to execute on Switch ON event.

    } else {
      setState(() {
        switchControl = false;
        textHolder = 'Offline';
      });
      print('Switch is OFF');
      // Put your code here which you want to execute on Switch OFF event.
    }
  }

  bool switchControl1 = false;
  var textHolder1 = 'You are Offline';
  void toggleSwitch1(bool value) {
    if (switchControl1 == false) {
      setState(() {
        switchControl1 = true;
        textHolder1 = 'You are Online';
        _showDialog() ;
      });
      print('Switch is ON');
      // Put your code here which you want to execute on Switch ON event.

    } else {
      setState(() {
        switchControl = false;
        textHolder1 = 'You are offline';
      });
      print('Switch is OFF');
      // Put your code here which you want to execute on Switch OFF event.
    }
  }

}


